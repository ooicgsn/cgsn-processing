#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@package cgsn_processing.process.proc_flort
@file cgsn_processing/process/proc_flort.py
@author Christopher Wingard
@brief Creates a NetCDF dataset for the FLORT from JSON formatted source data
ppw 02082024 - updated to support turbidity calculations, as follows:
Utilize the optional --switch parameter to support processing flort data to produce ONLY turbidity. 
Makes use of new calibration files being pushed to asset-management, having the naming scheme 
“CGINS-TURBDX-{s/n}__{date}.csv. These will live in a separate TURBDX subdirectory in asset 
management, per the CGSN data team. The value of the --switch flag should be “TURBDX” to compute 
turbidity; otherwise, standard FLORT outputs are calculated. The shell scripts for running 
processors or both parsers and processors have been updated accordingly.
"""
import numpy as np
import os
import pandas as pd
import re

from gsw import SP_from_C, p_from_z
from pocean.utils import dict_update
from pocean.dsg.timeseries.om import OrthogonalMultidimensionalTimeseries as OMTs
from scipy.interpolate import interp1d

from cgsn_processing.process.common import Coefficients, inputs, json2df, df2omtdf
from cgsn_processing.process.finding_calibrations import find_calibration
from cgsn_processing.process.configs.attr_flort import FLORT, TURBD

from pyseas.data.flo_functions import flo_scale_and_offset, flo_bback_total


class Calibrations(Coefficients):
    def __init__(self, coeff_file, csv_url=None):
        """
        Loads the FLORT factory calibration coefficients for a unit. Values come from either a serialized object
        created per instrument and deployment (calibration coefficients do not change in the middle of a deployment),
        or from parsed CSV files maintained on GitHub by the OOI CI team.
        """
        # assign the inputs
        Coefficients.__init__(self, coeff_file)
        self.csv_url = csv_url

    def read_csv(self, csv_url):
        """
        Reads the values from an ECO Triplet (aka FLORT) device file already parsed and stored on
        Github as a CSV files. Note, the formatting of those files puts some constraints on this process. 
        If someone has a cleaner method, I'm all in favor...
        """
        # create the device file dictionary and assign values
        coeffs = {}

        # read in the calibration data
        cal = pd.read_csv(csv_url, usecols=[0, 1, 2])
        for idx, row in cal.iterrows():
            # scale and offset correction factors
            if row.iloc[1] == 'CC_dark_counts_cdom':
                coeffs['dark_cdom'] = row.iloc[2]
            if row.iloc[1] == 'CC_scale_factor_cdom':
                coeffs['scale_cdom'] = row.iloc[2]

            if row.iloc[1] == 'CC_dark_counts_chlorophyll_a':
                coeffs['dark_chla'] = row.iloc[2]
            if row.iloc[1] == 'CC_scale_factor_chlorophyll_a':
                coeffs['scale_chla'] = row.iloc[2]

            if row.iloc[1] == 'CC_dark_counts_volume_scatter':
                coeffs['dark_beta'] = row.iloc[2]
            if row.iloc[1] == 'CC_scale_factor_volume_scatter':
                coeffs['scale_beta'] = row.iloc[2]

            # optical backscatter correction factors
            if row.iloc[1] == 'CC_angular_resolution':
                coeffs['chi_factor'] = row.iloc[2]
            if row.iloc[1] == 'CC_measurement_wavelength':
                coeffs['wavelength'] = row.iloc[2]
            if row.iloc[1] == 'CC_scattering_angle':
                coeffs['scatter_angle'] = row.iloc[2]

            # turbidity calculation factors
            if row.iloc[1] == 'CC_dark_counts_turbd':
                coeffs['dark_turbd'] = int(row.iloc[2])
            if row.iloc[1] == 'CC_scale_factor_turbd':
                coeffs['scale_turbd'] = float(row.iloc[2])

        # save the resulting dictionary
        self.coeffs = coeffs


def main(argv=None):
    # load  the input arguments
    args = inputs(argv)
    infile = os.path.abspath(args.infile)
    outfile = os.path.abspath(args.outfile)
    platform = args.platform
    deployment = args.deployment
    lat = args.latitude
    lon = args.longitude
    depth = args.depth
    ctd_name = args.devfile     # name of co-located CTD
    
    # add support for turbidity calculation - ppw02022024
    compute_turbidity_only = False
    calib_prefix = "FLORT"
    if args.switch is not None:
        if args.switch == "TURBDX" :
            compute_turbidity_only = True
            calib_prefix = "TURBDX"

    # load the json data file and return a panda dataframe
    df = json2df(infile)
    if df.empty:
        # there was no data in this file, ending early
        return None

    # remove the FLORT date/time string from the dataset
    _ = df.pop('flort_date_time_string')

    # check for the source of calibration coeffs and load accordingly
    coeff_file = os.path.abspath(args.coeff_file)
    dev = Calibrations(coeff_file)  # initialize calibration class
    if os.path.isfile(coeff_file):
        # we always want to use this file if it exists
        dev.load_coeffs()
    else:
        # load from the CI hosted CSV files
        csv_url = find_calibration(calib_prefix, args.serial, (df.time.values.astype('int64') * 10 ** -9)[0])
        if csv_url:
            dev.read_csv(csv_url)
            dev.save_coeffs()
        else:
            print('A source for the FLORT or TURBDX calibration coefficients for {} could not be found'.format(infile))
            return None

    if not compute_turbidity_only:
        # Apply the scale and offset correction factors from the factory calibration coefficients
        df['estimated_chlorophyll'] = flo_scale_and_offset(df['raw_signal_chl'], dev.coeffs['dark_chla'],
                                                           dev.coeffs['scale_chla'])
        df['fluorometric_cdom'] = flo_scale_and_offset(df['raw_signal_cdom'], dev.coeffs['dark_cdom'],
                                                       dev.coeffs['scale_cdom'])
        df['beta_700'] = flo_scale_and_offset(df['raw_signal_beta'], dev.coeffs['dark_beta'], dev.coeffs['scale_beta'])

        # Merge co-located CTD temperature and salinity data and calculate the total optical backscatter
        flort_path, flort_file = os.path.split(infile)
        ctd_file = re.sub('flort[\w]*', ctd_name, flort_file)
        ctd_path = re.sub('flort', re.sub('[\d]*', '', ctd_name), flort_path)
        ctd = json2df(os.path.join(ctd_path, ctd_file))
        if not ctd.empty and len(ctd.index) >= 3:
            # The Global moorings may use the data from the METBK-CT for FLORT mounted on the buoy subsurface plate.
            # We'll rename the data columns from the METBK to match other CTDs and process accordingly.
            if re.match('metbk', ctd_name):
                # rename temperature and salinity
                ctd = ctd.rename(columns={
                    'sea_surface_temperature': 'temperature',
                    'sea_surface_conductivity': 'conductivity'
                })
                # set the depth in dbar from the measured depth in m below the water line.
                if re.match('metbk1', ctd_name):
                    ctd['pressure'] = p_from_z(-1.3661, lat)
                elif re.match('metbk2', ctd_name):
                    ctd['pressure'] = p_from_z(-1.2328, lat)
                else:  # default of 1.00 m
                    ctd['pressure'] = p_from_z(-1.0000, lat)

            # calculate the practical salinity of the seawater from the temperature, conductivity and
            # pressure measurements
            ctd['psu'] = SP_from_C(ctd['conductivity'].values * 10.0, ctd['temperature'].values, ctd['pressure'].values)

            # interpolate temperature and salinity data from the CTD into the FLORT record for calculations
            degC = interp1d(ctd.time.values.astype('int64'), ctd.temperature.values, bounds_error=False)
            df['temperature'] = degC(df.time.values.astype('int64'))

            psu = interp1d(ctd.time.values.astype('int64'), ctd.psu, bounds_error=False)
            df['salinity'] = psu(df.time.values.astype('int64'))

            df['bback'] = flo_bback_total(df['beta_700'], df['temperature'], df['salinity'], 124., 700., 1.076)
        else:
            df['temperature'] = -9999.9
            df['salinity'] = -9999.9
            df['bback'] = -9999.9

    else:  # turbidity only

        # Apply the scale and offset correction factors from the factory calibration coefficients
        df['turbidity'] = flo_scale_and_offset(df['raw_signal_beta'], dev.coeffs['dark_turbd'],
                                               dev.coeffs['scale_turbd'])
                
    # convert the dataframe to a format suitable for the pocean OMTs
    df['deploy_id'] = deployment
    df = df2omtdf(df, lat, lon, depth)

    # Setup and update the attributes for the resulting NetCDF file
    if not compute_turbidity_only:
        attr = FLORT
    else:
        attr = TURBD
    attr['global'] = dict_update(attr['global'], {
        'comment': 'Mooring ID: {}-{}'.format(platform.upper(), re.sub('\D', '', deployment))
    })

    nc = OMTs.from_dataframe(df, outfile, attributes=attr)
    nc.close()


if __name__ == '__main__':
    main()
